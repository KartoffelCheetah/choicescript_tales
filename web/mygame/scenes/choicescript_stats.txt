Your character so far in the story:

*if (pl1_name12 != "")
  *stat_chart
    text pl1_name12 Name
    percent pl1_hp @{(round((pl1_hp/20))+1) close to passing out | weak | bad shape | tired | healthy | very healthy}
    percent pl1_mp Magic

*comment *if (pl1_height != 0)
*comment   *if pl1_height = 50
*comment     I'm as average height as it's possible. 179.5 cm is perfectly average for a human at this age and day. Having an average stature means I do not get unwanted attention.
*comment   *elseif (pl1_height > 50)
*comment     I'm taller than most humans. I have to be careful at home not to hit my head in the lamp. On the flipside animals tend to regard me with more respect. Some humans too.
*comment   *elseif (pl1_height < 50)
*comment     I'm quite short. I'm good at disappearing in crowds when the need arises, crawling into tiny spaces, you name it. Sadly I'm also good at being lost in crowds.
